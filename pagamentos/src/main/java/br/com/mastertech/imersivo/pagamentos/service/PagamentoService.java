package br.com.mastertech.imersivo.pagamentos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.pagamentos.client.CartaoClient;
import br.com.mastertech.imersivo.pagamentos.model.Pagamento;
import br.com.mastertech.imersivo.pagamentos.repository.PagamentoRepository;
import feign.FeignException;

@Service
public class PagamentoService {

	@Autowired
	private PagamentoRepository pagamentoRepository;
	
	@Autowired
	private CartaoClient cartaoClient;
	
	public Pagamento criarPagamento(Pagamento pagamento) {
		try {
			cartaoClient.buscaCartaoPorId(pagamento.getCartaoId());
		} catch (FeignException.NotFound e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cartão não encontrado");
		}
		
		return pagamentoRepository.save(pagamento);
	}
	
	public List<Pagamento> buscaPagamentos(Long cartao_id) {
		try {
			cartaoClient.buscaCartaoPorId(cartao_id);
		} catch (FeignException.NotFound e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cartão não encontrado");
		}
		return pagamentoRepository.findAllByCartaoId(cartao_id);
	}
	
	
}
